CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Links


INTRODUCTION
------------

The main goal of this module is to count downloads of files using AJAX.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/2947522 for further information.

 * Or You can install it using composer,
   $ composer require drupal/ajax_file_download_counter

 * You can also install it using drupal console cli,
   $ drupal module:install ajax_file_download_counter


CONFIGURATION
-------------

 * In Manage display, change file format as Ajax Download Counter.

 * Configure the number of hours to keep history here:
   - admin/config/ajax_dlcount


MAINTAINERS
-----------

 * iyyappan govind (iyyappan.govind) - https://www.drupal.org/u/iyyappangovind-0
 * Magesh Sekar (mageshsoft) - https://www.drupal.org/u/mageshsoft
 * saranya purushothaman - https://www.drupal.org/u/saranya-purushothaman


LINKS
-----

Project page: https://www.drupal.org/project/ajax_file_download_counter
Documentation: https://www.drupal.org/node/2947522
Submit bug reports, feature suggestions: https://www.drupal.org/project/issues/ajax_file_download_counter
