<?php

namespace Drupal\ajax_file_download_counter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;

/**
 * Defines controller for ajax_dlcount
 */
class AjaxDownloadCounter extends ControllerBase{

	/**
	 * {@inheritdoc}
	 */
	public function IncreaseCount($fid,$nid){
		//error_log("ajax_dlcount_count: $fid");
		$entity = entity_load('file', $fid);
		if(!empty($entity)){
			$count = 0;
			if(isset($entity->field_file_download_count)){
				$entity->field_file_download_count->value += 1;
			}
			$entity->save();
			$count = $entity->field_file_download_count->value;
		}
		if($nid) {
      Node::load($nid)->save();
    }
	  $params = array('dlcount' => $count);
	  return new JsonResponse($params);
	}
}